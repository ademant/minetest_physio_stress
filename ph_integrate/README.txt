Minetest Game mod: ph_integrate
==========================
See license.txt for license information.

This mod integrates items of other mods for using with mod physio_stress.

Several eating items are enhanced with thirst value, leading to simultaneous increasing of saturation and thirst, e.g. an apple gives you also some water.

Authors of source code
----------------------
ademant (MIT)

Authors of media (textures)
---------------------------
  
Created by ademant (CC BY 3.0):
